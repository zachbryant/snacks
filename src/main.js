import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Icon from 'vue-awesome/components/Icon'
import 'vue-awesome/icons'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
Vue.component('faIcon', Icon)

Vue.config.productionTip = false
Vue.use(Vuetify, {
  theme: {
    primary: '#ee44aa',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
